//Bài 1
function b1_tinhluong() {
  let luong1ngay = 100000;
  let songay = document.getElementById("b1_soNgayLam").value;
  let luong = luong1ngay * songay;
  document.getElementById("b1_ketQua").innerHTML = "Lương của bạn là: " + luong;
}

//Bài 2
function b2_tinhtb() {
  let b2_so1 = document.getElementById("b2_so1").value;
  let b2_so2 = document.getElementById("b2_so2").value;
  let b2_so3 = document.getElementById("b2_so3").value;
  let b2_so4 = document.getElementById("b2_so4").value;
  let b2_so5 = document.getElementById("b2_so5").value;

  let tb =
    (Number(b2_so1) +
      Number(b2_so2) +
      Number(b2_so3) +
      Number(b2_so4) +
      Number(b2_so5)) /
    5;
  document.getElementById("b2_ketQua").innerHTML = "Kết quả là: " + tb;
}

//Bài 3
function b3_quyDoi() {
  let usd = 23500;
  let b3_soTien = document.getElementById("b3_soTien").value;
  let vnd = usd * b3_soTien;
  document.getElementById("b3_ketQua").innerHTML = `Kết quả là: ${vnd} VND`;
}

//Bài 4
function b4_tinh() {
  let b4_chieudai = document.getElementById("b4_chieudai").value;
  let b4_chieurong = document.getElementById("b4_chieurong").value;
  let chuvi = (Number(b4_chieudai) + Number(b4_chieurong)) * 2;
  let dientich = Number(b4_chieudai) * Number(b4_chieurong);
  document.getElementById(
    "b4_ketQua"
  ).innerHTML = `Chu vi là: ${chuvi} <br> Diện tích là: ${dientich}`;
}

//Bài 5
function b5_tinh() {
  let b5_so = document.getElementById("b5_so").value;

  if (b5_so.length != 2) {
    document.getElementById("b5_ketQua").innerHTML = `Số bạn nhập không hợp lệ`;
  } else {
    b5_so = Number(b5_so);
    let soHangChuc = Math.floor(b5_so / 10);
    let soHangDonVi = b5_so % 10;
    let tong = soHangChuc + soHangDonVi;
    document.getElementById("b5_ketQua").innerHTML = `Tổng là: ${tong}`;
  }
}
